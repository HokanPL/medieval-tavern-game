﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A class of a cubic dice.
/// </summary>
public class Dice : MonoBehaviour
{
    /// <summary>
    /// A collection of potential dice's owners.
    /// </summary>
    public enum Player { playerOne, playerTwo}

    /// <summary>
    /// An owner of a dice.
    /// </summary>
    public Player player;

    /// <summary>
    /// A variable which specifies if a throwed dice has stable position on table or not.
    /// </summary>
    public bool isStable = false;

    private Rigidbody mybody;

    private void Awake()
    {
        mybody = GetComponent<Rigidbody>();
    }

    private IEnumerator CheckIfMyPositionIsStable()
    {
        do
        {
            Vector3 previousPosition = transform.position;
            yield return new WaitForSeconds(1f);
            Vector3 currentPosition = transform.position;
            isStable = (previousPosition == currentPosition);
        }
        while (!isStable);
    }
    /// <summary>
    /// A method which apllies force to a throwed dice in random point in area of this dice. It provides randomization.
    /// </summary>
    public void ApplyForce()
    {
        StopCoroutine("CheckIfMyPositionIsStable");

        float throwForce = Random.Range(10f, 12f);

        Vector3 pointOfForce = new Vector3();
        pointOfForce.x = Random.Range(-transform.localScale.x, transform.localScale.x);
        pointOfForce.y = Random.Range(-transform.localScale.y, transform.localScale.y);
        pointOfForce.z = Random.Range(-transform.localScale.z, transform.localScale.z);

        Vector3 rotationSpeed = new Vector3();
        rotationSpeed.x = Random.Range(-20f, 20f);
        rotationSpeed.y = Random.Range(-20f, 20f);
        rotationSpeed.z = Random.Range(-20f, 20f);

        mybody.velocity = Vector3.zero;
        mybody.angularVelocity = Vector3.zero;

        switch (player)
        {
            case Player.playerOne:
                mybody.AddForceAtPosition(Vector3.forward * throwForce, transform.localPosition + pointOfForce, ForceMode.Impulse);
                break;
            case Player.playerTwo:
                mybody.AddForceAtPosition(Vector3.back * throwForce, transform.localPosition + pointOfForce, ForceMode.Impulse);
                break;
            default:
                Debug.Log("Error: Player not recognized.");
                break;
        }
        mybody.AddTorque(rotationSpeed, ForceMode.Impulse);

        StartCoroutine("CheckIfMyPositionIsStable");
    }

    private void OnCollisionEnter(Collision collision)
    {
        AudioSource sound = gameObject.GetComponent<AudioSource>();
        sound.Play();
    }
}
