﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A class of a scanner which detects the face of a dice on top.
/// </summary>
public class DiceScanner : MonoBehaviour
{

    /// <summary>
    /// A scanned dice.
    /// </summary>
    public Dice myDice;

    /// <summary>
    /// A series of possible types of dice's faces that scanner can point to.
    /// </summary>
    public enum Face
    {
        _blank,
        archer,
        horseman,
        pikeman,
        shielder,
        swordsman
    };
    /// <summary>
    /// A face of a dice that is pointed by scanner.
    /// </summary>
    public Face pointedFace;

    private void Update()
    {
        UpdatePosition();
        ScanDiceFace();
    }

    /// <summary>
    /// Updates position of a scanner which has to be exactly over myDice during scanning.
    /// </summary>
    public void UpdatePosition()
    {
        Vector3 newPosition = new Vector3();
        newPosition.x = myDice.transform.position.x;
        newPosition.y = myDice.transform.position.y + 50f;
        newPosition.z = myDice.transform.position.z;

        transform.position = newPosition;
    }


    /// <summary>
    /// Scans a face of dice which is on top.
    /// </summary>
    public void ScanDiceFace()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, -transform.up, out hit, 60f))
        {
            if (hit.collider.tag == "CubeFace")
            {
                UpdateFaceData(hit.collider.gameObject.GetComponent<DiceFace>());
            }
        }
    }

    private void UpdateFaceData(DiceFace _face)
    {
        switch (_face.myFace)
        {
            case DiceFace.Face._blank:
                pointedFace = Face._blank;
                break;
            case DiceFace.Face.archer:
                pointedFace = Face.archer;
                break;
            case DiceFace.Face.horseman:
                pointedFace = Face.horseman;
                break;
            case DiceFace.Face.pikeman:
                pointedFace = Face.pikeman;
                break;
            case DiceFace.Face.shielder:
                pointedFace = Face.shielder;
                break;
            case DiceFace.Face.swordsman:
                pointedFace = Face.swordsman;
                break;
            default:
                Debug.Log("Error, face of a dice not recognized");
                break;
        }
    }
}
