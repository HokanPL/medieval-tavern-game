﻿using UnityEngine;


/// <summary>
/// A class of a spawnpoint which stores position and rotation data of its <seealso cref="Dice"/> before throwing.
/// </summary>
public class Spawnpoint : MonoBehaviour
{
    /// <summary>
    /// A dice of a spawnpoint.
    /// </summary>
    public Dice myDice;

    private void Awake()
    {
        myDice.gameObject.SetActive(false);
    }

    /// <summary>
    /// A method to throw a dice.
    /// </summary>
    public void ThrowADice()
    {
        myDice.transform.position = transform.position;
        myDice.transform.rotation = transform.rotation;
        myDice.gameObject.SetActive(true);
        myDice.ApplyForce();
    }
}
