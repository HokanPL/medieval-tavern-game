﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A class of a singleton Gameplay Manager, which stores information about current state of gameplay.
/// </summary>
[RequireComponent(typeof(TotalResult))]
public class GameplayManager : MonoBehaviour
{
    private static GameplayManager _instance;
    /// <summary>
    /// An instance of a Gameplay Manager singleton.
    /// </summary>
    public static GameplayManager Instance { get { return _instance; } }

    private bool canPlayer1Throw;
    private bool canPlayer2Throw;
    private bool didPlayer1Throw;
    private bool didPlayer2Throw;

    /// <summary>
    /// A collection of Player1's dices' spawnpoints.
    /// </summary>
    public Spawnpoint[] spawnpointsPlayer1;

    /// <summary>
    /// A collection of Player2's dices' spawnpoints.
    /// </summary>
    public Spawnpoint[] spawnpointsPlayer2;

    #region Start game's Data

    /// <summary>
    /// Total number of rounds.
    /// </summary>
    [Header("Basic data")]
    public int numberOfRounds = 5;
    
    /// <summary>
    /// A start amount of player1 (human) money.
    /// </summary>
    public int pocketStartPlayer1 = 200;
    
    /// <summary>
    /// A start amount of player2 (AI) money.
    /// </summary>
    public int pocketStartPlayer2 = 200;
    
    /// <summary>
    /// An amount of money bet by all players.
    /// </summary>
    public int moneyInGame;
    #endregion

    #region Choice of avaliable bets
    
    /// <summary>
    /// A value of small bet.
    /// </summary>
    [Header("Choice of avaliable bets")]
    public int smallBetValue = 20;
    
    /// <summary>
    /// A value of average bet.
    /// </summary>
    public int averageBetValue = 60;
    
    /// <summary>
    /// A value of high bet.
    /// </summary>
    public int riskyBetValue = 100;
    #endregion

    #region Current game data

    /// <summary>
    /// A current amount of player1 (human) money.
    /// </summary>
    [HideInInspector] public int pocketCurrentPlayer1;
    
    /// <summary>
    /// A current amount of player2 (AI) money.
    /// </summary>
    [HideInInspector] public int pocketCurrentPlayer2;

    /// <summary>
    /// A number of rounds to finish a game.
    /// </summary>
    [HideInInspector] public int roundsLeft;

    /// <summary>
    /// An amount of player1's (human's) points.
    /// </summary>
    [HideInInspector] public int totalScorePlayer1;
    
    /// <summary>
    /// An amount of player2's (AI's) points.
    /// </summary>
    [HideInInspector] public int totalScorePlayer2;
    #endregion

    private void Awake()
    {
        if (_instance != null && _instance != this) Destroy(gameObject);
        else _instance = this;

        ResetGameplayData();
        InvokeRepeating("UpdateMe", 1f, 0.2f);
    }

    private void UpdateMe()
    {
        if (moneyInGame == 0)
        {
            if (pocketCurrentPlayer1 < 20) UserInterfaceManager.Instance.loseImage.gameObject.SetActive(true);
            else if (pocketCurrentPlayer2 < 20) UserInterfaceManager.Instance.winImage.gameObject.SetActive(true);
        }

        ThrowingDices();
    }


    private void ThrowingDices()
    {
        didPlayer1Throw = true;
        foreach (Spawnpoint s in spawnpointsPlayer1)
        {//jeżeli wszystkie kości są włączone i wszystkie się nie ruszają, to znaczy, że rzut został wykonany
            //jeśli którakolwiek kość jest wyłączona lub którakolwiek się rusza, rzut jest w toku
            if (s.myDice.gameObject.activeSelf == false || s.myDice.isStable == false)
            {
                didPlayer1Throw = false;
                break;
            }
        }

        if (didPlayer1Throw)
        {
            foreach (Spawnpoint s in spawnpointsPlayer1)
            {
                s.myDice.gameObject.SetActive(false);
                s.myDice.isStable = false;
            }
            ThrowDices(spawnpointsPlayer2);
        }

        didPlayer2Throw = true;
        foreach (Spawnpoint s in spawnpointsPlayer2)
        {
            if (s.myDice.gameObject.activeSelf == false || s.myDice.isStable == false)
            {
                didPlayer2Throw = false;
                break;
            }
        }

        if (didPlayer2Throw)
        {
            foreach (Spawnpoint s in spawnpointsPlayer2)
            {
                s.myDice.gameObject.SetActive(false);
                s.myDice.isStable = false;
            }
            totalScorePlayer1 += gameObject.GetComponent<TotalResult>().scorePlayer1;
            totalScorePlayer2 += gameObject.GetComponent<TotalResult>().scorePlayer2;
            NextRound();
        }
    }


    private void NextRound()
    {
        roundsLeft--;
        if (roundsLeft <= 0)
        {
            roundsLeft = 0;
            //compareResults and transfer money
            if (totalScorePlayer1 > totalScorePlayer2)
            {
                pocketCurrentPlayer1 += moneyInGame;
            }
            else if (totalScorePlayer1 < totalScorePlayer2)
            {
                pocketCurrentPlayer2 += moneyInGame;
            }
            else
            {
                pocketCurrentPlayer1 += moneyInGame/2;
                pocketCurrentPlayer2 += moneyInGame/2;
            }

            int _pocket1 = pocketCurrentPlayer1;
            int _pocket2 = pocketCurrentPlayer2;

            ResetGameplayData();

            pocketCurrentPlayer1 = _pocket1;
            pocketCurrentPlayer2 = _pocket2;

            UserInterfaceManager.Instance.betSmallButton.gameObject.SetActive(true);
            UserInterfaceManager.Instance.betAverageButton.gameObject.SetActive(true);
            UserInterfaceManager.Instance.betRiskyButton.gameObject.SetActive(true);
        }
        else
        {
            UserInterfaceManager.Instance.throwDicesButton.gameObject.SetActive(true);
        }
    }
    /// <summary>
    /// Throws a collection of dices.
    /// </summary>
    public void ThrowDices(Spawnpoint[] _spawnpoints)
    {
        foreach (Spawnpoint s in _spawnpoints)
        {
            s.ThrowADice();
        }
    }

    /// <summary>
    /// Sets a bet.
    /// </summary>
    /// <param name="_value">A value of the bet</param>
    public void SetBet(int _value)
    {
        pocketCurrentPlayer1 -= _value;
        pocketCurrentPlayer2 -= _value;
        moneyInGame += 2*_value;
    }

    /// <summary>
    /// Restores data to begin a new game.
    /// </summary>
    public void ResetGameplayData()
    {
        didPlayer1Throw = false;
        didPlayer2Throw = false;

        pocketCurrentPlayer1 = pocketStartPlayer1;
        pocketCurrentPlayer2 = pocketStartPlayer2;
        moneyInGame = 0;
        roundsLeft = numberOfRounds;
        totalScorePlayer1 = 0;
        totalScorePlayer2 = 0;
    }
}
