﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// A class which stores result of throwing a dice.
/// </summary>
[RequireComponent(typeof(Image))]
public class DiceResult : MonoBehaviour
{
    /// <summary>
    /// A dice scanner.
    /// </summary>
    public DiceScanner myDiceScanner;
    /// <summary>
    /// A collection of unit images.
    /// </summary>
    public Sprite[] images;
    private Image myImage;

    /// <summary>
    /// Possible types of units on dice's faces.
    /// </summary>
    public enum UnitType
    {
        _blank,
        archer,
        horseman,
        pikeman,
        shielder,
        swordsman
    }
    /// <summary>
    /// A type of unit shown by a dice.
    /// </summary>
    public UnitType type;

    private void Awake()
    {
        myImage = gameObject.GetComponent<Image>();
    }

    private void Update()
    {
        UpdateUnitType();
        UpdateImage();
    }

    /// <summary>
    /// Updates <see cref="type"/> of unit shown by a dice.
    /// </summary>
    public void UpdateUnitType()
    {
        switch (myDiceScanner.pointedFace)
        {
            case DiceScanner.Face._blank:
                type = UnitType._blank;
                break;
            case DiceScanner.Face.archer:
                type = UnitType.archer;
                break;
            case DiceScanner.Face.horseman:
                type = UnitType.horseman;
                break;
            case DiceScanner.Face.pikeman:
                type = UnitType.pikeman;
                break;
            case DiceScanner.Face.shielder:
                type = UnitType.shielder;
                break;
            case DiceScanner.Face.swordsman:
                type = UnitType.swordsman;
                break;
            default:
                Debug.Log("Error: Unit type not recognized.");
                break;
        }
    }

    /// <summary>
    /// Updates <see cref="myImage"/> based on <see cref="type"/> of unit.
    /// </summary>
    public void UpdateImage()
    {
        switch (type)
        {
            case UnitType._blank:
                myImage.sprite = images[0];
                break;
            case UnitType.archer:
                myImage.sprite = images[1];
                break;
            case UnitType.horseman:
                myImage.sprite = images[2];
                break;
            case UnitType.pikeman:
                myImage.sprite = images[3];
                break;
            case UnitType.shielder:
                myImage.sprite = images[4];
                break;
            case UnitType.swordsman:
                myImage.sprite = images[5];
                break;
            default:
                Debug.Log("Error: Image of unit can't be updated.");
                break;
        }
    }
}
